from bs4 import BeautifulSoup
from requests import get
import re

pattern = re.compile(r'/images/(news|program)/site_1/2015/.*', re.IGNORECASE)
numberset = [str(i + 25000) for i in range(1001)]
head = 'http://www.europaplus.ru/index.php?go=News&in=view&id='
f = open('success.txt', 'w')
count = 0
for num in numberset:
    url = head + num
    pg = get(url).content
    soup = BeautifulSoup(pg, 'html.parser')
    soup = soup.body
    x = len(soup.findAll('img', src=pattern, alt=''))
    if x > 0:
        f.write(str(x) + ' --- ' + url + '\n')

    percent = (int(num) - 25000) / 90
    if percent > count:
        print(str(count) + '% complete')
        count += 1
