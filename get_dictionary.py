from bs4 import BeautifulSoup
from requests import get
import pickle

if __name__ == "__main__":
    src_num = {}
    urls = open("urls.txt", "r")
    for url in urls:
        url = "http://" + url[:-1]
        pg = get(url).content
        soup = BeautifulSoup(pg, "html.parser").body
        x = len(soup.findAll('img'))
        src_num[url[7:]] = x
        print(url[7:], x)
    with open("dict.pickle", "wb") as f:
        pickle.dump(src_num, f)
    urls.close()


# iconv -f ISO-8859-1 -t UTF-8 index.html > index.html
